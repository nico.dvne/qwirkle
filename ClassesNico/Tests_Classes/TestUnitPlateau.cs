﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;

namespace Tests_Classes
{
    [TestClass]
    public class TestUnitPlateau
    {
        [TestMethod]
        public void TestPutsPiece()
        {
            Piece tuile = new Piece("Bleu", "Etoile");
            Plateau plateau_de_jeu = new Plateau();
            Plateau.PutsPieceHere(15, 15, tuile);
            Assert.AreEqual(true,Plateau.ThereIsSomeone(15, 15));
        }
        [TestMethod]
        public void TestRemove()
        {
            Piece tuile = new Piece("Bleu", "Etoile");
            Plateau plateau_de_jeu = new Plateau();
            Plateau.PutsPieceHere(15, 15, tuile);
            Assert.AreEqual(true, Plateau.ThereIsSomeone(15, 15));
            Plateau.RemovePiece(15, 15);
            Assert.AreEqual(false, Plateau.ThereIsSomeone(15, 15));
        }
        [TestMethod]
        public void TestThereIs()
        {
            Piece tuile = new Piece("Bleu", "Etoile");
            Plateau plateau_de_jeu = new Plateau();
            Assert.AreEqual(false, Plateau.ThereIsSomeone(15, 15));
            Plateau.PutsPieceHere(15, 15, tuile);
            Assert.AreEqual(true, Plateau.ThereIsSomeone(15, 15));
        }
    }
}
