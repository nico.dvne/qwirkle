﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;

namespace Tests_Classes
{
    [TestClass]
    public class TestUnitaireJoueur
    {
        [TestMethod]
        public void TestGetSet()
        {
            Joueur Nicolas = new Joueur("Nicolas", 18);
            Assert.AreEqual("Nicolas", Nicolas.getName());
            Assert.AreEqual(18, Nicolas.getAge());
            Assert.AreNotEqual(15, Nicolas.getAge());
            Assert.AreEqual(0, Nicolas.getScore());
            Nicolas.setOrdre(4);
            Assert.AreEqual(4, Nicolas.getOrdre());
        }
        [TestMethod]
        public void TestPoser()
        {
            
            //simple test de la methode
            Joueur Nicolas = new Joueur("Nicolas", 18);
            Piece carrebleu = new Piece("bleu", "carre");            
            Nicolas.PlacerPiece(carrebleu, 5, 5);            
            Assert.AreEqual(true, Plateau.AccessCase(5, 5).GetStatut());            
            Assert.AreEqual("bleu", Plateau.AccessCase(5, 5).GetPiece().GetColor());
            Assert.AreEqual("carre", Plateau.AccessCase(5, 5).GetPiece().GetShape());

            //Test de l'impossibilité de poser une autre piece sur cette case
            Piece cerclejaune = new Piece("jaune", "cercle");
            Nicolas.PlacerPiece(cerclejaune, 5, 5);
            Assert.AreNotEqual("jaune", Plateau.AccessCase(5, 5).GetPiece().GetColor());
            Assert.AreNotEqual("cercle", Plateau.AccessCase(5, 5).GetPiece().GetShape());
            Assert.AreEqual("bleu", Plateau.AccessCase(5, 5).GetPiece().GetColor());
            Assert.AreEqual("carre", Plateau.AccessCase(5, 5).GetPiece().GetShape());
        }
        [TestMethod]
        public void TestEnlever()
        {
            //Test de enlever
            Joueur Nicolas = new Joueur("Nicolas", 18);
            Piece carrebleu = new Piece("Bleu", "Carre");
            Nicolas.PlacerPiece(carrebleu, 5, 5);
            Assert.AreEqual(true, Plateau.AccessCase(5, 5).GetStatut());
            Nicolas.EnleverPieceDuPlateau(carrebleu, 5, 5);
            Assert.IsFalse(Plateau.AccessCase(5, 5).GetStatut());
            Assert.IsNull(Plateau.AccessCase(5, 5).GetPiece());

           
        }
        
        
    }
}
