﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;

namespace Tests_Classes
{
    [TestClass]
    public class TestCases
    {
        [TestMethod]
        public void TestGetPosition()
        {
            Case case1 = new Case(5, 5);
            Assert.AreEqual(5, case1.GetPositionX());
            Assert.AreEqual(5, case1.GetPositionY());
        }
        [TestMethod]
        public void TestsSetPiece()
        {
            Assert.AreEqual(false, Plateau.AccessCase(5, 5).GetStatut());
            Piece carréBleu = new Piece("bleu", "carre");
            Plateau.AccessCase(5, 5).SetPiece(carréBleu);
            Assert.AreEqual(true, Plateau.AccessCase(5, 5).GetStatut());
            

        }
        [TestMethod]
        public void TestsRemovePiece()
        {
            Plateau.AccessCase(5, 5).RemovePiece();
            Assert.AreEqual(false, Plateau.AccessCase(5, 5).GetStatut());
            Assert.AreEqual(false, Plateau.AccessCase(6, 6).GetStatut());
            Piece carreVert = new Piece("vert", "carre");
            Plateau.AccessCase(6, 6).SetPiece(carreVert);
            Assert.AreEqual(true, Plateau.AccessCase(6, 6).GetStatut());
            Plateau.AccessCase(6, 6).RemovePiece();
            Assert.AreEqual(false, Plateau.AccessCase(6, 6).GetStatut());
        }
    }
}
