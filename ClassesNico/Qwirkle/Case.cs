﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qwirkle
{
    public class Case
    {
        private bool occupee; //si pas de piece dans la case => false, si pièce dans la case =>true


        ////////////////////////////////////////////////////////
        
        private int Position_X; /* Ces deux valeurs correspondent a la position */
        private int Position_Y; /* de la case dans le tableau de pièce*/

        private Piece Piece_Contenu;

     ////////////////////////////////////////////////////////

        
        public Case(int valueX , int valueY)
        {            
                 //gerer que pas de valueX,valueY superieure a 20 , 20
                this.occupee = false;
                this.Position_X = valueX;
                this.Position_Y = valueY;
                this.Piece_Contenu = null; //indique que pas de piece dans la case a l'initialisation du tableau
                
            
        }
        public int GetPositionX()
        {
            return this.Position_X;
        }     
       
        public int GetPositionY()
        {
            return this.Position_Y;
        }  
        public bool GetStatut()
        {
            return this.occupee;
        }
        public Piece GetPiece()
        {
            return this.Piece_Contenu;
        }
        public void SetStatut(bool value)
        {
            if (this.occupee = !value)
                this.occupee = value;
        }
        public void SetPiece(Piece example) //"Poser piece" dans la case
        {
            this.Piece_Contenu = example;
            this.occupee = true;
            
        }
        public void RemovePiece()
        {
           if(this.GetStatut()==true)
            {
                this.Piece_Contenu = null;
                this.occupee = false;      

            }
        }

    }
}
