﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qwirkle
{
    public class Pioche
    {
        private static List<Piece> tas_de_piece;

        static Pioche()
        {
            tas_de_piece = Piece.CreationPiece();
        }

         public static Piece Piocher()
        {
            int taille = Pioche.tas_de_piece.Count;
            Random aleatoire = new Random();
            int tirage = aleatoire.Next(0, taille);
            Piece a_renvoyer = tas_de_piece[tirage];
            tas_de_piece.Remove(tas_de_piece[tirage]);
            return a_renvoyer;
        }
        public static Piece Echanger(Piece a_echanger)
        {
            //Le retirer du deck
            tas_de_piece.Add(a_echanger);
            return Piocher();
        }
        public static void Ajouter(Piece a_ajouter)
        {
            tas_de_piece.Add(a_ajouter);
        }
        public static void Supprimer(Piece a_supprimer)
        {
            tas_de_piece.Remove(a_supprimer);
        }

    }
}
