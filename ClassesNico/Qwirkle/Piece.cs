﻿using System;
using System.Collections.Generic;

namespace Qwirkle
{
    public class Piece
    {
        private string color;
        private string shape;

        public Piece(string color, string shape)
        {
            this.color = color;
            this.shape = shape;
        }
        public string GetColor()
        {
            return this.color;
        }
        public string GetShape()
        {
            return this.shape;
        }
        public static List<Piece> CreationPiece()
        {
            string[] tabCouleur = { "Bleu", "Rouge", "Vert", "Jaune", "Orange", "Violet" };
            string[] tabForme = { "Carre", "Cercle", "Etoile", "Trefle", "Croix", "Losange" };
            List<Piece> pioche = new List<Piece>();
            for(int i = 0; i<6; i++)
            {
                for(int j = 0; j<6; j++)
                {
                    for(int k = 0; k<3; k++)
                    {
                        pioche.Add(new Piece(tabCouleur[i], tabForme[j]));
                    }
                }
            }
            return pioche;
        }
    }
}
