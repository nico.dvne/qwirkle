﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qwirkle
{
    public class Plateau
    {
        private static  Case[,] game_board;


        /*Constructeur du Plateau*/
        static Plateau()
        {
            //Creation d'un nouveau tableau de case ( initialisée a NULL )
            game_board = new Case[20,20];
            
            //Creation de 20x20 cases du game_board 
            for (int increment = 0; increment<20; increment++)
            {
                for(int increment2 = 0; increment2 < 20; increment2++)
                {
                    game_board[increment, increment2] = new Case(increment, increment2);
                }
            }
        }


        /*Retourne true si il y a une piece dans la case de coordonnes (X,Y), false si pas de piece dans la case*/
        public static bool ThereIsSomeone(int coordonnesX, int coordonnesY)
        {
            return game_board[coordonnesX, coordonnesY].GetStatut();           
        }


        /*Sert a poser une pièce dans la case selectionnee*/
        public static void PutsPieceHere(int coordonnesX , int coordonnesY,Piece example)
        {
            if(ThereIsSomeone(coordonnesX,coordonnesY)==false)
            {                
                game_board[coordonnesX, coordonnesY].SetPiece(example);
            }
        }


        public static void RemovePiece(int coordonnesX, int coordonnesY)
        {
            if(ThereIsSomeone(coordonnesX,coordonnesY)==true)
            {               
                game_board[coordonnesX, coordonnesY].RemovePiece();
            }
        }
        public static Case AccessCase(int coordonnes_X, int coordonnes_Y)
        {
            return game_board[coordonnes_X, coordonnes_Y];
        }
    }
    
}
