﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Qwirkle
{
    public class Joueur
    {

        private string nom;
        private int age;
        
        private int score;
        private List<Piece> deck; //Il y aura 6 piece dedans

        
        
        public Joueur(string nom, int age)
        {
            this.nom = nom;
            this.age = age;
            this.deck = new List<Piece>();
            for (int increment = 0; increment<6;increment++)
            {
                deck.Add(Pioche.Piocher());
            }
            this.score = 0;
            
        }

        public string getName()
        {
            return this.nom;
        }

        public int getAge()
        {
            return this.age;
        }

        

        public void setScore(int nombre)
        {
            this.score = nombre;
        }
        public List<Piece> GetDeck()
        {
            return this.deck;
        }

        public int getScore()
        {
            return this.score;
        }  
        public void PlacerPiece(Piece piece_a_placer,int x,int y)
        {
            Piece intermediaire = piece_a_placer;
            this.deck.Remove(piece_a_placer);
            Plateau.PutsPieceHere(x, y, intermediaire);
           
        }
        public void EnleverPieceDuPlateau(Piece piece_a_suppr,int x , int y)
        {
            Piece intermediaire = piece_a_suppr;
            this.deck.Add(piece_a_suppr);
            Plateau.RemovePiece(x, y);
        }    
           



    }
}
