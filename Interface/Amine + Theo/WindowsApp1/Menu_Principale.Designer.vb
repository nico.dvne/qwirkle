﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Menu_Principale
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Menu_Principale))
        Me.btn_regle = New System.Windows.Forms.Button()
        Me.btn_new = New System.Windows.Forms.Button()
        Me.btn_quitter = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_regle
        '
        Me.btn_regle.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btn_regle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_regle.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.btn_regle.Location = New System.Drawing.Point(437, 359)
        Me.btn_regle.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btn_regle.Name = "btn_regle"
        Me.btn_regle.Size = New System.Drawing.Size(173, 78)
        Me.btn_regle.TabIndex = 7
        Me.btn_regle.Text = "Règle du jeu"
        Me.btn_regle.UseVisualStyleBackColor = True
        '
        'btn_new
        '
        Me.btn_new.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btn_new.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_new.Font = New System.Drawing.Font("Century Gothic", 13.0!)
        Me.btn_new.Location = New System.Drawing.Point(437, 232)
        Me.btn_new.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btn_new.Name = "btn_new"
        Me.btn_new.Size = New System.Drawing.Size(173, 78)
        Me.btn_new.TabIndex = 5
        Me.btn_new.Text = "Nouvelle partie"
        Me.btn_new.UseVisualStyleBackColor = True
        '
        'btn_quitter
        '
        Me.btn_quitter.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btn_quitter.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_quitter.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.btn_quitter.Location = New System.Drawing.Point(437, 497)
        Me.btn_quitter.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btn_quitter.Name = "btn_quitter"
        Me.btn_quitter.Size = New System.Drawing.Size(173, 78)
        Me.btn_quitter.TabIndex = 4
        Me.btn_quitter.Text = "Quitter"
        Me.btn_quitter.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = CType(resources.GetObject("PictureBox1.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox1.Location = New System.Drawing.Point(228, 31)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(588, 163)
        Me.PictureBox1.TabIndex = 20
        Me.PictureBox1.TabStop = False
        '
        'Menu_Principale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1086, 658)
        Me.ControlBox = False
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.btn_regle)
        Me.Controls.Add(Me.btn_new)
        Me.Controls.Add(Me.btn_quitter)
        Me.Name = "Menu_Principale"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Menu"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btn_regle As Button
    Friend WithEvents btn_new As Button
    Friend WithEvents btn_quitter As Button
    Friend WithEvents PictureBox1 As PictureBox
End Class
