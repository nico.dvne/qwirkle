﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TableauJeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TableauJeu))
        Me.grpJ2 = New System.Windows.Forms.GroupBox()
        Me.lbl_point_J2 = New System.Windows.Forms.Label()
        Me.lbl_J2 = New System.Windows.Forms.Label()
        Me.lbl_pts2 = New System.Windows.Forms.Label()
        Me.PicJ2_6 = New System.Windows.Forms.PictureBox()
        Me.PicJ2_5 = New System.Windows.Forms.PictureBox()
        Me.PicJ2_4 = New System.Windows.Forms.PictureBox()
        Me.PicJ2_2 = New System.Windows.Forms.PictureBox()
        Me.PicJ2_3 = New System.Windows.Forms.PictureBox()
        Me.PicJ2_1 = New System.Windows.Forms.PictureBox()
        Me.grpJ1 = New System.Windows.Forms.GroupBox()
        Me.lbl_point_J1 = New System.Windows.Forms.Label()
        Me.lbl_J1 = New System.Windows.Forms.Label()
        Me.lbl_pts1 = New System.Windows.Forms.Label()
        Me.PicJ1_6 = New System.Windows.Forms.PictureBox()
        Me.PicJ1_5 = New System.Windows.Forms.PictureBox()
        Me.PicJ1_4 = New System.Windows.Forms.PictureBox()
        Me.PicJ1_2 = New System.Windows.Forms.PictureBox()
        Me.PicJ1_3 = New System.Windows.Forms.PictureBox()
        Me.PicJ1_1 = New System.Windows.Forms.PictureBox()
        Me.grpJ4 = New System.Windows.Forms.GroupBox()
        Me.lbl_point_J4 = New System.Windows.Forms.Label()
        Me.PicJ4_6 = New System.Windows.Forms.PictureBox()
        Me.PicJ4_5 = New System.Windows.Forms.PictureBox()
        Me.PicJ4_4 = New System.Windows.Forms.PictureBox()
        Me.PicJ4_3 = New System.Windows.Forms.PictureBox()
        Me.PicJ4_2 = New System.Windows.Forms.PictureBox()
        Me.lbl_pts4 = New System.Windows.Forms.Label()
        Me.lbl_J4 = New System.Windows.Forms.Label()
        Me.PicJ4_1 = New System.Windows.Forms.PictureBox()
        Me.lbl_J3 = New System.Windows.Forms.Label()
        Me.lbl_Joueur_joue = New System.Windows.Forms.Label()
        Me.grpJ3 = New System.Windows.Forms.GroupBox()
        Me.lbl_point_J3 = New System.Windows.Forms.Label()
        Me.PicJ3_5 = New System.Windows.Forms.PictureBox()
        Me.lbl_pts3 = New System.Windows.Forms.Label()
        Me.PicJ3_4 = New System.Windows.Forms.PictureBox()
        Me.PicJ3_3 = New System.Windows.Forms.PictureBox()
        Me.PicJ3_2 = New System.Windows.Forms.PictureBox()
        Me.PicJ3_6 = New System.Windows.Forms.PictureBox()
        Me.PicJ3_1 = New System.Windows.Forms.PictureBox()
        Me.lbl_test = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Btn_Retour = New System.Windows.Forms.Button()
        Me.cmd_echanger = New System.Windows.Forms.Button()
        Me.cmd_quitter = New System.Windows.Forms.Button()
        Me.cmd_passer = New System.Windows.Forms.Button()
        Me.cmd_valider = New System.Windows.Forms.Button()
        Me.cmd_annuler = New System.Windows.Forms.Button()
        Me.grpJ2.SuspendLayout
        CType(Me.PicJ2_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ2_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ2_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ2_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ2_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ2_1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpJ1.SuspendLayout()
        CType(Me.PicJ1_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ1_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ1_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ1_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ1_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ1_1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpJ4.SuspendLayout()
        CType(Me.PicJ4_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ4_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ4_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ4_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ4_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ4_1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpJ3.SuspendLayout()
        CType(Me.PicJ3_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ3_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ3_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ3_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ3_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicJ3_1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grpJ2
        '
        Me.grpJ2.Controls.Add(Me.lbl_point_J2)
        Me.grpJ2.Controls.Add(Me.lbl_J2)
        Me.grpJ2.Controls.Add(Me.lbl_pts2)
        Me.grpJ2.Controls.Add(Me.PicJ2_6)
        Me.grpJ2.Controls.Add(Me.PicJ2_5)
        Me.grpJ2.Controls.Add(Me.PicJ2_4)
        Me.grpJ2.Controls.Add(Me.PicJ2_2)
        Me.grpJ2.Controls.Add(Me.PicJ2_3)
        Me.grpJ2.Controls.Add(Me.PicJ2_1)
        resources.ApplyResources(Me.grpJ2, "grpJ2")
        Me.grpJ2.Name = "grpJ2"
        Me.grpJ2.TabStop = False
        '
        'lbl_point_J2
        '
        resources.ApplyResources(Me.lbl_point_J2, "lbl_point_J2")
        Me.lbl_point_J2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lbl_point_J2.Name = "lbl_point_J2"
        '
        'lbl_J2
        '
        resources.ApplyResources(Me.lbl_J2, "lbl_J2")
        Me.lbl_J2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lbl_J2.Name = "lbl_J2"
        '
        'lbl_pts2
        '
        resources.ApplyResources(Me.lbl_pts2, "lbl_pts2")
        Me.lbl_pts2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lbl_pts2.Name = "lbl_pts2"
        '
        'PicJ2_6
        '
        Me.PicJ2_6.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ2_6, "PicJ2_6")
        Me.PicJ2_6.Name = "PicJ2_6"
        Me.PicJ2_6.TabStop = False
        '
        'PicJ2_5
        '
        Me.PicJ2_5.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ2_5, "PicJ2_5")
        Me.PicJ2_5.Name = "PicJ2_5"
        Me.PicJ2_5.TabStop = False
        '
        'PicJ2_4
        '
        Me.PicJ2_4.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ2_4, "PicJ2_4")
        Me.PicJ2_4.Name = "PicJ2_4"
        Me.PicJ2_4.TabStop = False
        '
        'PicJ2_2
        '
        Me.PicJ2_2.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ2_2, "PicJ2_2")
        Me.PicJ2_2.Name = "PicJ2_2"
        Me.PicJ2_2.TabStop = False
        '
        'PicJ2_3
        '
        Me.PicJ2_3.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ2_3, "PicJ2_3")
        Me.PicJ2_3.Name = "PicJ2_3"
        Me.PicJ2_3.TabStop = False
        '
        'PicJ2_1
        '
        Me.PicJ2_1.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ2_1, "PicJ2_1")
        Me.PicJ2_1.Name = "PicJ2_1"
        Me.PicJ2_1.TabStop = False
        '
        'grpJ1
        '
        Me.grpJ1.Controls.Add(Me.lbl_point_J1)
        Me.grpJ1.Controls.Add(Me.lbl_J1)
        Me.grpJ1.Controls.Add(Me.lbl_pts1)
        Me.grpJ1.Controls.Add(Me.PicJ1_6)
        Me.grpJ1.Controls.Add(Me.PicJ1_5)
        Me.grpJ1.Controls.Add(Me.PicJ1_4)
        Me.grpJ1.Controls.Add(Me.PicJ1_2)
        Me.grpJ1.Controls.Add(Me.PicJ1_3)
        Me.grpJ1.Controls.Add(Me.PicJ1_1)
        resources.ApplyResources(Me.grpJ1, "grpJ1")
        Me.grpJ1.Name = "grpJ1"
        Me.grpJ1.TabStop = False
        '
        'lbl_point_J1
        '
        resources.ApplyResources(Me.lbl_point_J1, "lbl_point_J1")
        Me.lbl_point_J1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lbl_point_J1.Name = "lbl_point_J1"
        '
        'lbl_J1
        '
        resources.ApplyResources(Me.lbl_J1, "lbl_J1")
        Me.lbl_J1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lbl_J1.Name = "lbl_J1"
        '
        'lbl_pts1
        '
        resources.ApplyResources(Me.lbl_pts1, "lbl_pts1")
        Me.lbl_pts1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lbl_pts1.Name = "lbl_pts1"
        '
        'PicJ1_6
        '
        Me.PicJ1_6.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ1_6, "PicJ1_6")
        Me.PicJ1_6.Name = "PicJ1_6"
        Me.PicJ1_6.TabStop = False
        '
        'PicJ1_5
        '
        Me.PicJ1_5.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ1_5, "PicJ1_5")
        Me.PicJ1_5.Name = "PicJ1_5"
        Me.PicJ1_5.TabStop = False
        '
        'PicJ1_4
        '
        Me.PicJ1_4.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ1_4, "PicJ1_4")
        Me.PicJ1_4.Name = "PicJ1_4"
        Me.PicJ1_4.TabStop = False
        '
        'PicJ1_2
        '
        Me.PicJ1_2.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ1_2, "PicJ1_2")
        Me.PicJ1_2.Name = "PicJ1_2"
        Me.PicJ1_2.TabStop = False
        '
        'PicJ1_3
        '
        Me.PicJ1_3.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ1_3, "PicJ1_3")
        Me.PicJ1_3.Name = "PicJ1_3"
        Me.PicJ1_3.TabStop = False
        '
        'PicJ1_1
        '
        Me.PicJ1_1.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ1_1, "PicJ1_1")
        Me.PicJ1_1.Name = "PicJ1_1"
        Me.PicJ1_1.TabStop = False
        '
        'grpJ4
        '
        Me.grpJ4.Controls.Add(Me.lbl_point_J4)
        Me.grpJ4.Controls.Add(Me.PicJ4_6)
        Me.grpJ4.Controls.Add(Me.PicJ4_5)
        Me.grpJ4.Controls.Add(Me.PicJ4_4)
        Me.grpJ4.Controls.Add(Me.PicJ4_3)
        Me.grpJ4.Controls.Add(Me.PicJ4_2)
        Me.grpJ4.Controls.Add(Me.lbl_pts4)
        Me.grpJ4.Controls.Add(Me.lbl_J4)
        Me.grpJ4.Controls.Add(Me.PicJ4_1)
        resources.ApplyResources(Me.grpJ4, "grpJ4")
        Me.grpJ4.Name = "grpJ4"
        Me.grpJ4.TabStop = False
        '
        'lbl_point_J4
        '
        resources.ApplyResources(Me.lbl_point_J4, "lbl_point_J4")
        Me.lbl_point_J4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lbl_point_J4.Name = "lbl_point_J4"
        '
        'PicJ4_6
        '
        Me.PicJ4_6.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ4_6, "PicJ4_6")
        Me.PicJ4_6.Name = "PicJ4_6"
        Me.PicJ4_6.TabStop = False
        '
        'PicJ4_5
        '
        Me.PicJ4_5.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ4_5, "PicJ4_5")
        Me.PicJ4_5.Name = "PicJ4_5"
        Me.PicJ4_5.TabStop = False
        '
        'PicJ4_4
        '
        Me.PicJ4_4.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ4_4, "PicJ4_4")
        Me.PicJ4_4.Name = "PicJ4_4"
        Me.PicJ4_4.TabStop = False
        '
        'PicJ4_3
        '
        Me.PicJ4_3.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ4_3, "PicJ4_3")
        Me.PicJ4_3.Name = "PicJ4_3"
        Me.PicJ4_3.TabStop = False
        '
        'PicJ4_2
        '
        Me.PicJ4_2.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ4_2, "PicJ4_2")
        Me.PicJ4_2.Name = "PicJ4_2"
        Me.PicJ4_2.TabStop = False
        '
        'lbl_pts4
        '
        resources.ApplyResources(Me.lbl_pts4, "lbl_pts4")
        Me.lbl_pts4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lbl_pts4.Name = "lbl_pts4"
        '
        'lbl_J4
        '
        resources.ApplyResources(Me.lbl_J4, "lbl_J4")
        Me.lbl_J4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lbl_J4.Name = "lbl_J4"
        '
        'PicJ4_1
        '
        Me.PicJ4_1.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ4_1, "PicJ4_1")
        Me.PicJ4_1.Name = "PicJ4_1"
        Me.PicJ4_1.TabStop = False
        '
        'lbl_J3
        '
        resources.ApplyResources(Me.lbl_J3, "lbl_J3")
        Me.lbl_J3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lbl_J3.Name = "lbl_J3"
        '
        'lbl_Joueur_joue
        '
        resources.ApplyResources(Me.lbl_Joueur_joue, "lbl_Joueur_joue")
        Me.lbl_Joueur_joue.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.lbl_Joueur_joue.Name = "lbl_Joueur_joue"
        '
        'grpJ3
        '
        Me.grpJ3.Controls.Add(Me.lbl_point_J3)
        Me.grpJ3.Controls.Add(Me.PicJ3_5)
        Me.grpJ3.Controls.Add(Me.lbl_pts3)
        Me.grpJ3.Controls.Add(Me.PicJ3_4)
        Me.grpJ3.Controls.Add(Me.PicJ3_3)
        Me.grpJ3.Controls.Add(Me.PicJ3_2)
        Me.grpJ3.Controls.Add(Me.lbl_J3)
        Me.grpJ3.Controls.Add(Me.PicJ3_6)
        Me.grpJ3.Controls.Add(Me.PicJ3_1)
        resources.ApplyResources(Me.grpJ3, "grpJ3")
        Me.grpJ3.Name = "grpJ3"
        Me.grpJ3.TabStop = False
        '
        'lbl_point_J3
        '
        resources.ApplyResources(Me.lbl_point_J3, "lbl_point_J3")
        Me.lbl_point_J3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lbl_point_J3.Name = "lbl_point_J3"
        '
        'PicJ3_5
        '
        Me.PicJ3_5.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ3_5, "PicJ3_5")
        Me.PicJ3_5.Name = "PicJ3_5"
        Me.PicJ3_5.TabStop = False
        '
        'lbl_pts3
        '
        resources.ApplyResources(Me.lbl_pts3, "lbl_pts3")
        Me.lbl_pts3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lbl_pts3.Name = "lbl_pts3"
        '
        'PicJ3_4
        '
        Me.PicJ3_4.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ3_4, "PicJ3_4")
        Me.PicJ3_4.Name = "PicJ3_4"
        Me.PicJ3_4.TabStop = False
        '
        'PicJ3_3
        '
        Me.PicJ3_3.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ3_3, "PicJ3_3")
        Me.PicJ3_3.Name = "PicJ3_3"
        Me.PicJ3_3.TabStop = False
        '
        'PicJ3_2
        '
        Me.PicJ3_2.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ3_2, "PicJ3_2")
        Me.PicJ3_2.Name = "PicJ3_2"
        Me.PicJ3_2.TabStop = False
        '
        'PicJ3_6
        '
        Me.PicJ3_6.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ3_6, "PicJ3_6")
        Me.PicJ3_6.Name = "PicJ3_6"
        Me.PicJ3_6.TabStop = False
        '
        'PicJ3_1
        '
        Me.PicJ3_1.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.PicJ3_1, "PicJ3_1")
        Me.PicJ3_1.Name = "PicJ3_1"
        Me.PicJ3_1.TabStop = False
        '
        'lbl_test
        '
        resources.ApplyResources(Me.lbl_test, "lbl_test")
        Me.lbl_test.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.lbl_test.Name = "lbl_test"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Name = "Label1"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Name = "Label2"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Name = "Label3"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Name = "Label4"
        '
        'Btn_Retour
        '
        Me.Btn_Retour.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        resources.ApplyResources(Me.Btn_Retour, "Btn_Retour")
        Me.Btn_Retour.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Retour.ForeColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Btn_Retour.Name = "Btn_Retour"
        Me.Btn_Retour.UseVisualStyleBackColor = False
        '
        'cmd_echanger
        '
        resources.ApplyResources(Me.cmd_echanger, "cmd_echanger")
        Me.cmd_echanger.Image = Global.WindowsApp1.My.Resources.Resources.BTNechanger
        Me.cmd_echanger.Name = "cmd_echanger"
        '
        'cmd_quitter
        '
        Me.cmd_quitter.BackgroundImage = Global.WindowsApp1.My.Resources.Resources.BTNexit
        resources.ApplyResources(Me.cmd_quitter, "cmd_quitter")
        Me.cmd_quitter.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmd_quitter.Name = "cmd_quitter"
        '
        'cmd_passer
        '
        resources.ApplyResources(Me.cmd_passer, "cmd_passer")
        Me.cmd_passer.Image = Global.WindowsApp1.My.Resources.Resources.BTNpasserTour
        Me.cmd_passer.Name = "cmd_passer"
        '
        'cmd_valider
        '
        resources.ApplyResources(Me.cmd_valider, "cmd_valider")
        Me.cmd_valider.Image = Global.WindowsApp1.My.Resources.Resources.BTNvalider
        Me.cmd_valider.Name = "cmd_valider"
        '
        'cmd_annuler
        '
        Me.cmd_annuler.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        resources.ApplyResources(Me.cmd_annuler, "cmd_annuler")
        Me.cmd_annuler.Image = Global.WindowsApp1.My.Resources.Resources.BTNretour
        Me.cmd_annuler.Name = "cmd_annuler"
        Me.cmd_annuler.UseVisualStyleBackColor = False
        '
        'TableauJeu
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Btn_Retour)
        Me.Controls.Add(Me.lbl_test)
        Me.Controls.Add(Me.grpJ2)
        Me.Controls.Add(Me.grpJ1)
        Me.Controls.Add(Me.grpJ4)
        Me.Controls.Add(Me.cmd_echanger)
        Me.Controls.Add(Me.cmd_quitter)
        Me.Controls.Add(Me.cmd_passer)
        Me.Controls.Add(Me.lbl_Joueur_joue)
        Me.Controls.Add(Me.cmd_valider)
        Me.Controls.Add(Me.grpJ3)
        Me.Controls.Add(Me.cmd_annuler)
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Name = "TableauJeu"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.grpJ2.ResumeLayout(False)
        Me.grpJ2.PerformLayout()
        CType(Me.PicJ2_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ2_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ2_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ2_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ2_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ2_1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpJ1.ResumeLayout(False)
        Me.grpJ1.PerformLayout()
        CType(Me.PicJ1_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ1_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ1_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ1_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ1_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ1_1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpJ4.ResumeLayout(False)
        Me.grpJ4.PerformLayout()
        CType(Me.PicJ4_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ4_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ4_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ4_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ4_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ4_1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpJ3.ResumeLayout(False)
        Me.grpJ3.PerformLayout()
        CType(Me.PicJ3_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ3_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ3_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ3_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ3_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicJ3_1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents PicJ1_4 As PictureBox
    Private WithEvents PicJ1_3 As PictureBox
    Private WithEvents PicJ1_1 As PictureBox
    Private WithEvents grpJ2 As GroupBox
    Private WithEvents lbl_J2 As Label
    Private WithEvents PicJ2_6 As PictureBox
    Private WithEvents PicJ2_5 As PictureBox
    Private WithEvents PicJ2_4 As PictureBox
    Private WithEvents PicJ2_2 As PictureBox
    Private WithEvents PicJ2_3 As PictureBox
    Private WithEvents PicJ2_1 As PictureBox
    Private WithEvents PicJ1_2 As PictureBox
    Private WithEvents PicJ1_5 As PictureBox
    Private WithEvents PicJ1_6 As PictureBox
    Private WithEvents grpJ1 As GroupBox
    Private WithEvents lbl_J1 As Label
    Private WithEvents PicJ3_1 As PictureBox
    Private WithEvents PicJ4_2 As PictureBox
    Private WithEvents PicJ4_6 As PictureBox
    Private WithEvents PicJ4_5 As PictureBox
    Private WithEvents PicJ4_4 As PictureBox
    Private WithEvents grpJ4 As GroupBox
    Private WithEvents PicJ4_3 As PictureBox
    Private WithEvents lbl_pts4 As Label
    Private WithEvents lbl_J4 As Label
    Private WithEvents PicJ4_1 As PictureBox
    Private WithEvents PicJ3_5 As PictureBox
    Private WithEvents PicJ3_3 As PictureBox
    Private WithEvents PicJ3_2 As PictureBox
    Private WithEvents lbl_J3 As Label
    Private WithEvents PicJ3_6 As PictureBox
    Private WithEvents PicJ3_4 As PictureBox
    Private WithEvents cmd_echanger As Button
    Private WithEvents cmd_quitter As Button
    Private WithEvents cmd_passer As Button
    Private WithEvents lbl_Joueur_joue As Label
    Private WithEvents cmd_valider As Button
    Private WithEvents grpJ3 As GroupBox
    Private WithEvents cmd_annuler As Button
    Private WithEvents lbl_point_J2 As Label
    Private WithEvents lbl_pts2 As Label
    Private WithEvents lbl_point_J1 As Label
    Private WithEvents lbl_pts1 As Label
    Private WithEvents lbl_point_J4 As Label
    Private WithEvents lbl_point_J3 As Label
    Private WithEvents lbl_pts3 As Label
    Private WithEvents lbl_test As Label
    Friend WithEvents Btn_Retour As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
End Class
