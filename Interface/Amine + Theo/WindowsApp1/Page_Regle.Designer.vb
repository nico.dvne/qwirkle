﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Page_Regle
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Page_Regle))
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lbl_p1 = New System.Windows.Forms.Label()
        Me.lbl_titre_regle = New System.Windows.Forms.Label()
        Me.btn_retour = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(189, 474)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(1026, 161)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = resources.GetString("Label3.Text")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(189, 393)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(865, 69)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Un joueur peut :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "- Jouer une ou plusieurs tuiles par tour (si elles partagent un" &
    "e caractéristique commune)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Dans ce cas, sa main est réalimentée d’autant de tui" &
    "les qu’il a joué."
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(189, 191)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(979, 161)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = resources.GetString("Label1.Text")
        '
        'lbl_p1
        '
        Me.lbl_p1.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.lbl_p1.ForeColor = System.Drawing.Color.White
        Me.lbl_p1.Location = New System.Drawing.Point(189, 156)
        Me.lbl_p1.Name = "lbl_p1"
        Me.lbl_p1.Size = New System.Drawing.Size(707, 34)
        Me.lbl_p1.TabIndex = 8
        Me.lbl_p1.Text = resources.GetString("lbl_p1.Text")
        '
        'lbl_titre_regle
        '
        Me.lbl_titre_regle.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.lbl_titre_regle.ForeColor = System.Drawing.Color.White
        Me.lbl_titre_regle.Location = New System.Drawing.Point(189, 82)
        Me.lbl_titre_regle.Name = "lbl_titre_regle"
        Me.lbl_titre_regle.Size = New System.Drawing.Size(183, 47)
        Me.lbl_titre_regle.TabIndex = 7
        Me.lbl_titre_regle.Text = "Règle du jeu :"
        '
        'btn_retour
        '
        Me.btn_retour.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_retour.Font = New System.Drawing.Font("Century Gothic", 8.25!)
        Me.btn_retour.Location = New System.Drawing.Point(1195, 690)
        Me.btn_retour.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_retour.Name = "btn_retour"
        Me.btn_retour.Size = New System.Drawing.Size(99, 38)
        Me.btn_retour.TabIndex = 6
        Me.btn_retour.Text = "Retour"
        Me.btn_retour.UseVisualStyleBackColor = True
        '
        'Regle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1484, 811)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lbl_p1)
        Me.Controls.Add(Me.lbl_titre_regle)
        Me.Controls.Add(Me.btn_retour)
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "Regle"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Regle"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents lbl_p1 As Label
    Friend WithEvents lbl_titre_regle As Label
    Friend WithEvents btn_retour As Button
End Class
