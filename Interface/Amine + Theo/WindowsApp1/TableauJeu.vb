﻿Imports Qwirkle
Public Class TableauJeu
    Private tiptool As New ToolTip()
    Private random As New Random()
    Private numero_image As Integer
    Private pic As PictureBox
    Private mongrp As GroupBox
    Private derniereTuile As PictureBox

    Private Sub cmd_valider_MouseHover(sender As Object, e As EventArgs) Handles cmd_valider.MouseHover
        tiptool.SetToolTip(cmd_valider, "Valider la pose")
    End Sub

    Private Sub cmd_annuler_MouseHover(sender As Object, e As EventArgs) Handles cmd_annuler.MouseHover
        tiptool.SetToolTip(cmd_annuler, "Annuler la dernière pose")
    End Sub

    Private Sub cmd_echanger_MouseHover(sender As Object, e As EventArgs) Handles cmd_echanger.MouseHover
        tiptool.SetToolTip(cmd_echanger, "Echanger tous les pions")
    End Sub

    Private Sub cmd_passer_MouseHover(sender As Object, e As EventArgs) Handles cmd_passer.MouseHover
        tiptool.SetToolTip(cmd_passer, "Passer le tour")
    End Sub

    Private Sub TableauJeu_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim lastpic As PictureBox
        Dim first_pic As Boolean = True
        Dim ligne As Integer
        Dim colonne As Integer
        Dim compteur1 As Integer
        Dim compteur2 As Integer
        Dim coord_X As Integer = 460, coord_Y As Integer = 100
        Dim nb_controls As Integer = Me.Controls.Count - 11
        lbl_J1.Text = NbJoueur.GetJoueur1().getName()
        lbl_J2.Text = NbJoueur.GetJoueur2().getName()
        lbl_J3.Text = NbJoueur.GetJoueur3().getName()
        lbl_J4.Text = NbJoueur.GetJoueur4().getName()
        lbl_point_J1.Text = CStr(NbJoueur.GetJoueur1().getScore())
        lbl_point_J2.Text = CStr(NbJoueur.GetJoueur2().getScore())
        lbl_point_J3.Text = CStr(NbJoueur.GetJoueur3().getScore())
        lbl_point_J4.Text = CStr(NbJoueur.GetJoueur4().getScore())

        'Création dynamique du tableau de jeu (20x20)
        For colonne = 0 To 19

            For ligne = 0 To 19

                Dim newpic As New PictureBox
                newpic.Name = "pic" & ligne & "_" & colonne
                newpic.Size = New Size(25, 25)
                newpic.Visible = True
                newpic.AllowDrop = True
                newpic.BackColor = Color.White
                newpic.BackgroundImageLayout = ImageLayout.Stretch

                If first_pic Then
                    newpic.Location = New Point(460, 100)
                    first_pic = False
                Else
                    newpic.Location = New Point(coord_X, coord_Y)
                End If

                Me.Controls.Add(newpic)

                AddHandler newpic.DragEnter, AddressOf Pic_Plateau_DragEnter
                AddHandler newpic.DragDrop, AddressOf Pic_Plateau_DragDrop

                lastpic = newpic
                coord_Y = lastpic.Location.Y + lastpic.Height + 5

            Next
            coord_X = coord_X + lastpic.Width + 5
            coord_Y = 100
        Next


        'Generation aleatoire de pions from J1 to J4
        For compteur1 = 1 To 4
            mongrp = Me.Controls("grpJ" & compteur1.ToString)
            For compteur2 = 1 To 6
                numero_image = random.Next(1, 36)
                pic = mongrp.Controls("PicJ" & compteur1.ToString & "_" & compteur2.ToString)
                pic.BackgroundImage = My.Resources.ResourceManager.GetObject("_" & numero_image)
            Next
        Next


        'Masquage des pions des joueurs qui ne jouent pas
        If (lbl_Joueur_joue.Text = "J1 joue") Then
            grpJ1.Visible = True
            grpJ2.Visible = False
            grpJ3.Visible = False
            grpJ4.Visible = False
        ElseIf (lbl_Joueur_joue.Text = "J2 joue") Then
            grpJ1.Visible = False
            grpJ2.Visible = True
            grpJ3.Visible = False
            grpJ4.Visible = False
        ElseIf (lbl_Joueur_joue.Text = "J3 joue") Then
            grpJ1.Visible = False
            grpJ2.Visible = False
            grpJ3.Visible = True
            grpJ4.Visible = False
        Else grpJ1.Visible = False
            grpJ2.Visible = False
            grpJ3.Visible = False
            grpJ4.Visible = True
        End If
    End Sub

    Private Sub Pic_Plateau_DragEnter(sender As Object, e As DragEventArgs)
        Dim pic As PictureBox = sender
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
        lbl_test.Text = pic.Name
    End Sub
    Private Sub Pic_Plateau_DragDrop(sender As Object, e As DragEventArgs)
        Dim pic As PictureBox = sender
        pic.BackgroundImage = e.Data.GetData(DataFormats.Bitmap)
        derniereTuile = pic
        sender.AllowDrop = False
    End Sub

    Private Sub PicJ3_1_MouseMove(sender As Object, e As MouseEventArgs) Handles PicJ4_6.MouseMove, PicJ4_5.MouseMove, PicJ4_4.MouseMove, PicJ4_2.MouseMove, PicJ4_1.MouseMove, PicJ4_3.MouseMove, PicJ3_6.MouseMove, PicJ3_4.MouseMove, PicJ3_3.MouseMove, PicJ3_2.MouseMove, PicJ3_1.MouseMove, PicJ2_6.MouseMove, PicJ2_5.MouseMove, PicJ2_4.MouseMove, PicJ2_3.MouseMove, PicJ2_2.MouseMove, PicJ2_1.MouseMove, PicJ1_6.MouseMove, PicJ1_5.MouseMove, PicJ1_4.MouseMove, PicJ1_3.MouseMove, PicJ1_2.MouseMove, PicJ1_1.MouseMove, PicJ3_5.MouseMove
        Dim effetRealise As DragDropEffects
        Dim pic As PictureBox = sender

        If e.Button = MouseButtons.Left AndAlso pic.BackgroundImage IsNot Nothing Then
            pic.AllowDrop = False
            effetRealise = pic.DoDragDrop(pic.BackgroundImage, DragDropEffects.Move)
            If effetRealise = DragDropEffects.Move Then
                pic.BackgroundImage = Nothing
            End If
            pic.AllowDrop = True
        End If
    End Sub




    Private Sub cmd_valider_Click(sender As Joueur, e As EventArgs) Handles cmd_valider.Click
        If (lbl_Joueur_joue.Text = "J1 joue") Then

            'Donne image random si la picturebox est vide
            mongrp = Me.Controls("grpJ1")
            For compteur2 = 1 To 6
                numero_image = random.Next(1, 36)
                pic = mongrp.Controls("PicJ1_" & compteur2.ToString)
                If pic.BackgroundImage Is Nothing Then
                    pic.BackgroundImage = My.Resources.ResourceManager.GetObject("_" & numero_image)
                End If
            Next

            lbl_Joueur_joue.Text = "J2 joue"

        ElseIf (lbl_Joueur_joue.Text = "J2 joue") Then

            mongrp = Me.Controls("grpJ2")
            For compteur2 = 1 To 6
                numero_image = random.Next(1, 36)
                pic = mongrp.Controls("PicJ2_" & compteur2.ToString)
                If pic.BackgroundImage Is Nothing Then
                    pic.BackgroundImage = My.Resources.ResourceManager.GetObject("_" & numero_image)
                End If
            Next

            lbl_Joueur_joue.Text = "J3 joue"

        ElseIf (lbl_Joueur_joue.Text = "J3 joue") Then

            mongrp = Me.Controls("grpJ3")
            For compteur2 = 1 To 6
                numero_image = random.Next(1, 36)
                pic = mongrp.Controls("PicJ3_" & compteur2.ToString)
                If pic.BackgroundImage Is Nothing Then
                    pic.BackgroundImage = My.Resources.ResourceManager.GetObject("_" & numero_image)
                End If
            Next

            lbl_Joueur_joue.Text = "J4 joue"

        Else

            mongrp = Me.Controls("grpJ4")
            For compteur2 = 1 To 6
                numero_image = random.Next(1, 36)
                pic = mongrp.Controls("PicJ4_" & compteur2.ToString)
                If pic.BackgroundImage Is Nothing Then
                    pic.BackgroundImage = My.Resources.ResourceManager.GetObject("_" & numero_image)


                End If
            Next

            lbl_Joueur_joue.Text = "J1 joue"

        End If

        'Masquage des pions des joueurs qui ne jouent pas
        If (lbl_Joueur_joue.Text = "J1 joue") Then
            grpJ1.Visible = True
            grpJ2.Visible = False
            grpJ3.Visible = False
            grpJ4.Visible = False
        ElseIf (lbl_Joueur_joue.Text = "J2 joue") Then
            grpJ1.Visible = False
            grpJ2.Visible = True
            grpJ3.Visible = False
            grpJ4.Visible = False
        ElseIf (lbl_Joueur_joue.Text = "J3 joue") Then
            grpJ1.Visible = False
            grpJ2.Visible = False
            grpJ3.Visible = True
            grpJ4.Visible = False
        Else grpJ1.Visible = False
            grpJ2.Visible = False
            grpJ3.Visible = False
            grpJ4.Visible = True
        End If

        derniereTuile = Nothing
    End Sub

    Private Sub cmd_passer_Click(sender As Object, e As EventArgs) Handles cmd_passer.Click

        If (lbl_Joueur_joue.Text = "J1 joue") Then

            'Donne image random si la picturebox est vide
            mongrp = Me.Controls("grpJ1")
            For compteur2 = 1 To 6
                numero_image = random.Next(1, 36)
                pic = mongrp.Controls("PicJ1_" & compteur2.ToString)
                If pic.BackgroundImage Is Nothing Then
                    pic.BackgroundImage = My.Resources.ResourceManager.GetObject("_" & numero_image)
                End If
            Next

            lbl_Joueur_joue.Text = "J2 joue"

        ElseIf (lbl_Joueur_joue.Text = "J2 joue") Then

            mongrp = Me.Controls("grpJ2")
            For compteur2 = 1 To 6
                numero_image = random.Next(1, 36)
                pic = mongrp.Controls("PicJ2_" & compteur2.ToString)
                If pic.BackgroundImage Is Nothing Then
                    pic.BackgroundImage = My.Resources.ResourceManager.GetObject("_" & numero_image)
                End If
            Next

            lbl_Joueur_joue.Text = "J3 joue"

        ElseIf (lbl_Joueur_joue.Text = "J3 joue") Then

            mongrp = Me.Controls("grpJ3")
            For compteur2 = 1 To 6
                numero_image = random.Next(1, 36)
                pic = mongrp.Controls("PicJ3_" & compteur2.ToString)
                If pic.BackgroundImage Is Nothing Then
                    pic.BackgroundImage = My.Resources.ResourceManager.GetObject("_" & numero_image)
                End If
            Next

            lbl_Joueur_joue.Text = "J4 joue"

        Else

            mongrp = Me.Controls("grpJ4")
            For compteur2 = 1 To 6
                numero_image = random.Next(1, 36)
                pic = mongrp.Controls("PicJ4_" & compteur2.ToString)
                If pic.BackgroundImage Is Nothing Then
                    pic.BackgroundImage = My.Resources.ResourceManager.GetObject("_" & numero_image)


                End If
            Next

            lbl_Joueur_joue.Text = "J1 joue"

        End If

        'Masquage des pions des joueurs qui ne jouent pas
        If (lbl_Joueur_joue.Text = "J1 joue") Then
            grpJ1.Visible = True
            grpJ2.Visible = False
            grpJ3.Visible = False
            grpJ4.Visible = False
        ElseIf (lbl_Joueur_joue.Text = "J2 joue") Then
            grpJ1.Visible = False
            grpJ2.Visible = True
            grpJ3.Visible = False
            grpJ4.Visible = False
        ElseIf (lbl_Joueur_joue.Text = "J3 joue") Then
            grpJ1.Visible = False
            grpJ2.Visible = False
            grpJ3.Visible = True
            grpJ4.Visible = False
        Else grpJ1.Visible = False
            grpJ2.Visible = False
            grpJ3.Visible = False
            grpJ4.Visible = True
        End If

        derniereTuile = Nothing
    End Sub

    Private Sub cmd_annuler_Click(sender As Object, e As EventArgs) Handles cmd_annuler.Click
        Dim first_annul As Boolean = True
        If derniereTuile Is Nothing Then

        Else
            If (lbl_Joueur_joue.Text = "J1 joue") Then

                mongrp = Me.Controls("grpJ1")
                For compteur2 = 1 To 6
                    pic = mongrp.Controls("PicJ1_" & compteur2.ToString)
                    If pic.BackgroundImage Is Nothing AndAlso first_annul Then
                        pic.BackgroundImage = derniereTuile.BackgroundImage
                        first_annul = False
                    End If
                Next
                first_annul = True

            ElseIf (lbl_Joueur_joue.Text = "J2 joue") Then

                mongrp = Me.Controls("grpJ2")
                For compteur2 = 1 To 6
                    pic = mongrp.Controls("PicJ2_" & compteur2.ToString)
                    If pic.BackgroundImage Is Nothing AndAlso first_annul Then
                        pic.BackgroundImage = derniereTuile.BackgroundImage
                        first_annul = False
                    End If
                Next
                first_annul = True

            ElseIf (lbl_Joueur_joue.Text = "J3 joue") Then

                mongrp = Me.Controls("grpJ3")
                For compteur2 = 1 To 6
                    pic = mongrp.Controls("PicJ3_" & compteur2.ToString)
                    If pic.BackgroundImage Is Nothing AndAlso first_annul Then
                        pic.BackgroundImage = derniereTuile.BackgroundImage
                        first_annul = False
                    End If
                Next
                first_annul = True

            Else

                mongrp = Me.Controls("grpJ4")
                For compteur2 = 1 To 6
                    pic = mongrp.Controls("PicJ4_" & compteur2.ToString)
                    If pic.BackgroundImage Is Nothing AndAlso first_annul Then
                        pic.BackgroundImage = derniereTuile.BackgroundImage
                        first_annul = False
                    End If
                Next
                first_annul = True
            End If
            derniereTuile.BackgroundImage = Nothing
            derniereTuile.AllowDrop = True
        End If
    End Sub

    Private Sub cmd_quitter_Click(sender As Object, e As EventArgs) Handles cmd_quitter.Click
        Me.Close()
    End Sub

    Private Sub cmd_echanger_Click(sender As Object, e As EventArgs) Handles cmd_echanger.Click
        If (lbl_Joueur_joue.Text = "J1 joue") Then

            'Donne image random si la picturebox est vide
            mongrp = Me.Controls("grpJ1")
            For compteur2 = 1 To 6
                pic = mongrp.Controls("PicJ1_" & compteur2.ToString)
                numero_image = random.Next(1, 36)
                pic.BackgroundImage = My.Resources.ResourceManager.GetObject("_" & numero_image)
            Next

            lbl_Joueur_joue.Text = "J2 joue"

        ElseIf (lbl_Joueur_joue.Text = "J2 joue") Then

            mongrp = Me.Controls("grpJ2")
            For compteur2 = 1 To 6
                pic = mongrp.Controls("PicJ2_" & compteur2.ToString)
                numero_image = random.Next(1, 36)
                pic.BackgroundImage = My.Resources.ResourceManager.GetObject("_" & numero_image)
            Next

            lbl_Joueur_joue.Text = "J3 joue"

        ElseIf (lbl_Joueur_joue.Text = "J3 joue") Then

            mongrp = Me.Controls("grpJ3")
            For compteur2 = 1 To 6
                pic = mongrp.Controls("PicJ3_" & compteur2.ToString)
                numero_image = random.Next(1, 36)
                pic.BackgroundImage = My.Resources.ResourceManager.GetObject("_" & numero_image)
            Next

            lbl_Joueur_joue.Text = "J4 joue"

        Else

            mongrp = Me.Controls("grpJ4")
            For compteur2 = 1 To 6
                pic = mongrp.Controls("PicJ4_" & compteur2.ToString)
                numero_image = random.Next(1, 36)
                pic.BackgroundImage = My.Resources.ResourceManager.GetObject("_" & numero_image)
            Next

            lbl_Joueur_joue.Text = "J1 joue"

        End If

        'Masquage des pions des joueurs qui ne jouent pas
        If (lbl_Joueur_joue.Text = "J1 joue") Then
            grpJ1.Visible = True
            grpJ2.Visible = False
            grpJ3.Visible = False
            grpJ4.Visible = False
        ElseIf (lbl_Joueur_joue.Text = "J2 joue") Then
            grpJ1.Visible = False
            grpJ2.Visible = True
            grpJ3.Visible = False
            grpJ4.Visible = False
        ElseIf (lbl_Joueur_joue.Text = "J3 joue") Then
            grpJ1.Visible = False
            grpJ2.Visible = False
            grpJ3.Visible = True
            grpJ4.Visible = False
        Else grpJ1.Visible = False
            grpJ2.Visible = False
            grpJ3.Visible = False
            grpJ4.Visible = True
        End If
    End Sub

End Class
