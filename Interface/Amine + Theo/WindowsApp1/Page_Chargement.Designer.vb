﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Page_Chargement
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Page_Chargement))
        Me.lbl_nom6 = New System.Windows.Forms.Label()
        Me.lbl_nom5 = New System.Windows.Forms.Label()
        Me.lbl_nom4 = New System.Windows.Forms.Label()
        Me.lbl_nom3 = New System.Windows.Forms.Label()
        Me.lbl_nom1 = New System.Windows.Forms.Label()
        Me.lbl_nom2 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lbl_value = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbl_nom6
        '
        Me.lbl_nom6.AutoSize = True
        Me.lbl_nom6.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.lbl_nom6.ForeColor = System.Drawing.Color.White
        Me.lbl_nom6.Location = New System.Drawing.Point(480, 371)
        Me.lbl_nom6.Name = "lbl_nom6"
        Me.lbl_nom6.Size = New System.Drawing.Size(96, 21)
        Me.lbl_nom6.TabIndex = 16
        Me.lbl_nom6.Text = "Amine Affif"
        '
        'lbl_nom5
        '
        Me.lbl_nom5.AutoSize = True
        Me.lbl_nom5.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.lbl_nom5.ForeColor = System.Drawing.Color.White
        Me.lbl_nom5.Location = New System.Drawing.Point(473, 339)
        Me.lbl_nom5.Name = "lbl_nom5"
        Me.lbl_nom5.Size = New System.Drawing.Size(110, 21)
        Me.lbl_nom5.TabIndex = 15
        Me.lbl_nom5.Text = "Gilbert Ineza"
        '
        'lbl_nom4
        '
        Me.lbl_nom4.AutoSize = True
        Me.lbl_nom4.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.lbl_nom4.ForeColor = System.Drawing.Color.White
        Me.lbl_nom4.Location = New System.Drawing.Point(468, 306)
        Me.lbl_nom4.Name = "lbl_nom4"
        Me.lbl_nom4.Size = New System.Drawing.Size(119, 21)
        Me.lbl_nom4.TabIndex = 14
        Me.lbl_nom4.Text = "Theo Boudard"
        '
        'lbl_nom3
        '
        Me.lbl_nom3.AutoSize = True
        Me.lbl_nom3.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.lbl_nom3.ForeColor = System.Drawing.Color.White
        Me.lbl_nom3.Location = New System.Drawing.Point(461, 274)
        Me.lbl_nom3.Name = "lbl_nom3"
        Me.lbl_nom3.Size = New System.Drawing.Size(134, 21)
        Me.lbl_nom3.TabIndex = 13
        Me.lbl_nom3.Text = "Maxime Ancher"
        '
        'lbl_nom1
        '
        Me.lbl_nom1.AutoSize = True
        Me.lbl_nom1.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.lbl_nom1.ForeColor = System.Drawing.Color.White
        Me.lbl_nom1.Location = New System.Drawing.Point(457, 209)
        Me.lbl_nom1.Name = "lbl_nom1"
        Me.lbl_nom1.Size = New System.Drawing.Size(142, 21)
        Me.lbl_nom1.TabIndex = 12
        Me.lbl_nom1.Text = "Nicolas Davenne"
        '
        'lbl_nom2
        '
        Me.lbl_nom2.AutoSize = True
        Me.lbl_nom2.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.lbl_nom2.ForeColor = System.Drawing.Color.White
        Me.lbl_nom2.Location = New System.Drawing.Point(459, 242)
        Me.lbl_nom2.Name = "lbl_nom2"
        Me.lbl_nom2.Size = New System.Drawing.Size(136, 21)
        Me.lbl_nom2.TabIndex = 11
        Me.lbl_nom2.Text = "Yacine Schamel"
        '
        'Timer1
        '
        '
        'lbl_value
        '
        Me.lbl_value.BackColor = System.Drawing.Color.Transparent
        Me.lbl_value.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.lbl_value.ForeColor = System.Drawing.Color.White
        Me.lbl_value.Location = New System.Drawing.Point(486, 464)
        Me.lbl_value.Name = "lbl_value"
        Me.lbl_value.Size = New System.Drawing.Size(100, 34)
        Me.lbl_value.TabIndex = 18
        Me.lbl_value.Text = "Label1"
        Me.lbl_value.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(146, 428)
        Me.ProgressBar1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(776, 34)
        Me.ProgressBar1.TabIndex = 17
        '
        'PictureBox3
        '
        Me.PictureBox3.BackgroundImage = Global.WindowsApp1.My.Resources.Resources.druite
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox3.Location = New System.Drawing.Point(916, 402)
        Me.PictureBox3.Margin = New System.Windows.Forms.Padding(0)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(44, 81)
        Me.PictureBox3.TabIndex = 21
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackgroundImage = Global.WindowsApp1.My.Resources.Resources.sffsfsGUCHE
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox2.Location = New System.Drawing.Point(107, 404)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(0)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(44, 78)
        Me.PictureBox2.TabIndex = 20
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = CType(resources.GetObject("PictureBox1.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox1.Location = New System.Drawing.Point(225, 20)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(588, 163)
        Me.PictureBox1.TabIndex = 19
        Me.PictureBox1.TabStop = False
        '
        'Page_Chargement
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1067, 589)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lbl_nom6)
        Me.Controls.Add(Me.lbl_nom5)
        Me.Controls.Add(Me.lbl_nom4)
        Me.Controls.Add(Me.lbl_nom3)
        Me.Controls.Add(Me.lbl_nom1)
        Me.Controls.Add(Me.lbl_nom2)
        Me.Controls.Add(Me.lbl_value)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Font = New System.Drawing.Font("Century Gothic", 7.8!)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "Page_Chargement"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Chargement"
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_nom6 As Label
    Friend WithEvents lbl_nom5 As Label
    Friend WithEvents lbl_nom4 As Label
    Friend WithEvents lbl_nom3 As Label
    Friend WithEvents lbl_nom1 As Label
    Friend WithEvents lbl_nom2 As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents lbl_value As Label
    Friend WithEvents ProgressBar1 As ProgressBar
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox3 As PictureBox
End Class
