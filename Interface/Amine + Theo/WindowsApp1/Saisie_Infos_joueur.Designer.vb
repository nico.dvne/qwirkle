﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class NbJoueur
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    Private Const V As Boolean = True

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NbJoueur))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ListeNbJoueurs = New System.Windows.Forms.ComboBox()
        Me.J1 = New System.Windows.Forms.Label()
        Me.J4 = New System.Windows.Forms.Label()
        Me.J2 = New System.Windows.Forms.Label()
        Me.J3 = New System.Windows.Forms.Label()
        Me.Pseudo1 = New System.Windows.Forms.TextBox()
        Me.Pseudo2 = New System.Windows.Forms.TextBox()
        Me.Pseudo3 = New System.Windows.Forms.TextBox()
        Me.Pseudo4 = New System.Windows.Forms.TextBox()
        Me.lblAGE3 = New System.Windows.Forms.Label()
        Me.lblAGE2 = New System.Windows.Forms.Label()
        Me.lblAGE4 = New System.Windows.Forms.Label()
        Me.lblAGE1 = New System.Windows.Forms.Label()
        Me.Age4 = New System.Windows.Forms.TextBox()
        Me.Age3 = New System.Windows.Forms.TextBox()
        Me.Age2 = New System.Windows.Forms.TextBox()
        Me.Age1 = New System.Windows.Forms.TextBox()
        Me.NbJoueurs = New System.Windows.Forms.Label()
        Me.BtnListeDeroul = New System.Windows.Forms.PictureBox()
        Me.Btn_Retour = New System.Windows.Forms.Button()
        Me.Btn_Next = New System.Windows.Forms.Button()
        Me.codingpcimage = New System.Windows.Forms.PictureBox()
        Me.Lbl_IA_en_cours_de_developpement = New System.Windows.Forms.Label()
        Me.Panel_sombre_couvrant = New System.Windows.Forms.Panel()
        Me.Lbl_Veuillez_nous_excuser = New System.Windows.Forms.Label()
        CType(Me.BtnListeDeroul, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.codingpcimage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel_sombre_couvrant.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 61.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(160, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(805, 98)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nombre de joueurs"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.Label2.ForeColor = System.Drawing.Color.SlateGray
        Me.Label2.Location = New System.Drawing.Point(301, 197)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(367, 43)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Pseudos des joueurs"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListeNbJoueurs
        '
        Me.ListeNbJoueurs.AutoCompleteCustomSource.AddRange(New String() {"?"})
        Me.ListeNbJoueurs.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.ListeNbJoueurs.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.ListeNbJoueurs.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ListeNbJoueurs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ListeNbJoueurs.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ListeNbJoueurs.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.ListeNbJoueurs.ForeColor = System.Drawing.Color.White
        Me.ListeNbJoueurs.FormattingEnabled = True
        Me.ListeNbJoueurs.Items.AddRange(New Object() {"1", "2", "3", "4"})
        Me.ListeNbJoueurs.Location = New System.Drawing.Point(184, 194)
        Me.ListeNbJoueurs.Name = "ListeNbJoueurs"
        Me.ListeNbJoueurs.Size = New System.Drawing.Size(78, 50)
        Me.ListeNbJoueurs.TabIndex = 2
        '
        'J1
        '
        Me.J1.AutoSize = True
        Me.J1.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.J1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.J1.Location = New System.Drawing.Point(286, 265)
        Me.J1.Name = "J1"
        Me.J1.Size = New System.Drawing.Size(56, 43)
        Me.J1.TabIndex = 3
        Me.J1.Text = "J1"
        Me.J1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'J4
        '
        Me.J4.AutoSize = True
        Me.J4.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.J4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.J4.Location = New System.Drawing.Point(286, 436)
        Me.J4.Name = "J4"
        Me.J4.Size = New System.Drawing.Size(56, 43)
        Me.J4.TabIndex = 4
        Me.J4.Text = "J4"
        Me.J4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'J2
        '
        Me.J2.AutoSize = True
        Me.J2.BackColor = System.Drawing.Color.Transparent
        Me.J2.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.J2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.J2.Location = New System.Drawing.Point(286, 322)
        Me.J2.Name = "J2"
        Me.J2.Size = New System.Drawing.Size(56, 43)
        Me.J2.TabIndex = 5
        Me.J2.Text = "J2"
        Me.J2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'J3
        '
        Me.J3.AutoSize = True
        Me.J3.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.J3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.J3.Location = New System.Drawing.Point(286, 379)
        Me.J3.Name = "J3"
        Me.J3.Size = New System.Drawing.Size(56, 43)
        Me.J3.TabIndex = 6
        Me.J3.Text = "J3"
        Me.J3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Pseudo1
        '
        Me.Pseudo1.BackColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.Pseudo1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Pseudo1.Enabled = False
        Me.Pseudo1.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.Pseudo1.Location = New System.Drawing.Point(348, 262)
        Me.Pseudo1.Name = "Pseudo1"
        Me.Pseudo1.Size = New System.Drawing.Size(293, 44)
        Me.Pseudo1.TabIndex = 7
        '
        'Pseudo2
        '
        Me.Pseudo2.BackColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.Pseudo2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Pseudo2.Enabled = False
        Me.Pseudo2.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.Pseudo2.Location = New System.Drawing.Point(348, 318)
        Me.Pseudo2.Name = "Pseudo2"
        Me.Pseudo2.Size = New System.Drawing.Size(293, 44)
        Me.Pseudo2.TabIndex = 8
        '
        'Pseudo3
        '
        Me.Pseudo3.BackColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.Pseudo3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Pseudo3.Enabled = False
        Me.Pseudo3.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.Pseudo3.Location = New System.Drawing.Point(348, 376)
        Me.Pseudo3.Name = "Pseudo3"
        Me.Pseudo3.Size = New System.Drawing.Size(293, 44)
        Me.Pseudo3.TabIndex = 9
        '
        'Pseudo4
        '
        Me.Pseudo4.BackColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.Pseudo4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Pseudo4.Enabled = False
        Me.Pseudo4.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.Pseudo4.Location = New System.Drawing.Point(348, 433)
        Me.Pseudo4.Name = "Pseudo4"
        Me.Pseudo4.Size = New System.Drawing.Size(293, 44)
        Me.Pseudo4.TabIndex = 10
        '
        'lblAGE3
        '
        Me.lblAGE3.AutoSize = True
        Me.lblAGE3.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.lblAGE3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.lblAGE3.Location = New System.Drawing.Point(646, 379)
        Me.lblAGE3.Name = "lblAGE3"
        Me.lblAGE3.Size = New System.Drawing.Size(91, 43)
        Me.lblAGE3.TabIndex = 14
        Me.lblAGE3.Text = "Âge"
        Me.lblAGE3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblAGE2
        '
        Me.lblAGE2.AutoSize = True
        Me.lblAGE2.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.lblAGE2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.lblAGE2.Location = New System.Drawing.Point(646, 322)
        Me.lblAGE2.Name = "lblAGE2"
        Me.lblAGE2.Size = New System.Drawing.Size(91, 43)
        Me.lblAGE2.TabIndex = 13
        Me.lblAGE2.Text = "Âge"
        Me.lblAGE2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblAGE4
        '
        Me.lblAGE4.AutoSize = True
        Me.lblAGE4.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.lblAGE4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.lblAGE4.Location = New System.Drawing.Point(646, 436)
        Me.lblAGE4.Name = "lblAGE4"
        Me.lblAGE4.Size = New System.Drawing.Size(91, 43)
        Me.lblAGE4.TabIndex = 12
        Me.lblAGE4.Text = "Âge"
        Me.lblAGE4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblAGE1
        '
        Me.lblAGE1.AutoSize = True
        Me.lblAGE1.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.lblAGE1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.lblAGE1.Location = New System.Drawing.Point(646, 265)
        Me.lblAGE1.Name = "lblAGE1"
        Me.lblAGE1.Size = New System.Drawing.Size(91, 43)
        Me.lblAGE1.TabIndex = 11
        Me.lblAGE1.Text = "Âge"
        Me.lblAGE1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Age4
        '
        Me.Age4.BackColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.Age4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Age4.Enabled = False
        Me.Age4.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.Age4.Location = New System.Drawing.Point(744, 433)
        Me.Age4.Name = "Age4"
        Me.Age4.Size = New System.Drawing.Size(142, 44)
        Me.Age4.TabIndex = 18
        '
        'Age3
        '
        Me.Age3.BackColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.Age3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Age3.Enabled = False
        Me.Age3.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.Age3.Location = New System.Drawing.Point(744, 376)
        Me.Age3.Name = "Age3"
        Me.Age3.Size = New System.Drawing.Size(142, 44)
        Me.Age3.TabIndex = 17
        '
        'Age2
        '
        Me.Age2.BackColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.Age2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Age2.Enabled = False
        Me.Age2.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.Age2.Location = New System.Drawing.Point(744, 318)
        Me.Age2.Name = "Age2"
        Me.Age2.Size = New System.Drawing.Size(142, 44)
        Me.Age2.TabIndex = 16
        '
        'Age1
        '
        Me.Age1.BackColor = System.Drawing.Color.FromArgb(CType(CType(6, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.Age1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Age1.Enabled = False
        Me.Age1.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.Age1.Location = New System.Drawing.Point(744, 262)
        Me.Age1.Name = "Age1"
        Me.Age1.Size = New System.Drawing.Size(142, 44)
        Me.Age1.TabIndex = 15
        '
        'NbJoueurs
        '
        Me.NbJoueurs.AutoSize = True
        Me.NbJoueurs.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.NbJoueurs.Cursor = System.Windows.Forms.Cursors.Hand
        Me.NbJoueurs.Font = New System.Drawing.Font("Century Gothic", 26.6!)
        Me.NbJoueurs.ForeColor = System.Drawing.Color.White
        Me.NbJoueurs.Location = New System.Drawing.Point(190, 194)
        Me.NbJoueurs.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.NbJoueurs.Name = "NbJoueurs"
        Me.NbJoueurs.Size = New System.Drawing.Size(40, 43)
        Me.NbJoueurs.TabIndex = 28
        Me.NbJoueurs.Text = "?"
        '
        'BtnListeDeroul
        '
        Me.BtnListeDeroul.BackgroundImage = CType(resources.GetObject("BtnListeDeroul.BackgroundImage"), System.Drawing.Image)
        Me.BtnListeDeroul.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.BtnListeDeroul.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnListeDeroul.Location = New System.Drawing.Point(170, 186)
        Me.BtnListeDeroul.Name = "BtnListeDeroul"
        Me.BtnListeDeroul.Size = New System.Drawing.Size(104, 59)
        Me.BtnListeDeroul.TabIndex = 22
        Me.BtnListeDeroul.TabStop = False
        '
        'Btn_Retour
        '
        Me.Btn_Retour.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Btn_Retour.BackgroundImage = CType(resources.GetObject("Btn_Retour.BackgroundImage"), System.Drawing.Image)
        Me.Btn_Retour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Btn_Retour.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Retour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Btn_Retour.ForeColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Btn_Retour.Location = New System.Drawing.Point(48, 496)
        Me.Btn_Retour.Name = "Btn_Retour"
        Me.Btn_Retour.Size = New System.Drawing.Size(120, 120)
        Me.Btn_Retour.TabIndex = 20
        Me.Btn_Retour.UseVisualStyleBackColor = False
        '
        'Btn_Next
        '
        Me.Btn_Next.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Btn_Next.BackgroundImage = CType(resources.GetObject("Btn_Next.BackgroundImage"), System.Drawing.Image)
        Me.Btn_Next.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Btn_Next.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Next.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Btn_Next.ForeColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Btn_Next.Location = New System.Drawing.Point(917, 496)
        Me.Btn_Next.Name = "Btn_Next"
        Me.Btn_Next.Size = New System.Drawing.Size(120, 120)
        Me.Btn_Next.TabIndex = 19
        Me.Btn_Next.UseVisualStyleBackColor = False
        '
        'codingpcimage
        '
        Me.codingpcimage.BackgroundImage = CType(resources.GetObject("codingpcimage.BackgroundImage"), System.Drawing.Image)
        Me.codingpcimage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.codingpcimage.Cursor = System.Windows.Forms.Cursors.Hand
        Me.codingpcimage.Location = New System.Drawing.Point(206, 235)
        Me.codingpcimage.Margin = New System.Windows.Forms.Padding(2)
        Me.codingpcimage.Name = "codingpcimage"
        Me.codingpcimage.Size = New System.Drawing.Size(374, 314)
        Me.codingpcimage.TabIndex = 31
        Me.codingpcimage.TabStop = False
        Me.codingpcimage.Visible = False
        '
        'Lbl_IA_en_cours_de_developpement
        '
        Me.Lbl_IA_en_cours_de_developpement.AutoSize = True
        Me.Lbl_IA_en_cours_de_developpement.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Lbl_IA_en_cours_de_developpement.Font = New System.Drawing.Font("Century Gothic", 30.0!)
        Me.Lbl_IA_en_cours_de_developpement.ForeColor = System.Drawing.Color.White
        Me.Lbl_IA_en_cours_de_developpement.Location = New System.Drawing.Point(198, 201)
        Me.Lbl_IA_en_cours_de_developpement.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Lbl_IA_en_cours_de_developpement.Name = "Lbl_IA_en_cours_de_developpement"
        Me.Lbl_IA_en_cours_de_developpement.Size = New System.Drawing.Size(735, 49)
        Me.Lbl_IA_en_cours_de_developpement.TabIndex = 30
        Me.Lbl_IA_en_cours_de_developpement.Text = "L' IA est en cours de développement"
        Me.Lbl_IA_en_cours_de_developpement.Visible = False
        '
        'Panel_sombre_couvrant
        '
        Me.Panel_sombre_couvrant.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Panel_sombre_couvrant.Controls.Add(Me.codingpcimage)
        Me.Panel_sombre_couvrant.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Panel_sombre_couvrant.Location = New System.Drawing.Point(161, 40)
        Me.Panel_sombre_couvrant.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel_sombre_couvrant.Name = "Panel_sombre_couvrant"
        Me.Panel_sombre_couvrant.Size = New System.Drawing.Size(725, 649)
        Me.Panel_sombre_couvrant.TabIndex = 29
        Me.Panel_sombre_couvrant.Visible = False
        '
        'Lbl_Veuillez_nous_excuser
        '
        Me.Lbl_Veuillez_nous_excuser.AutoSize = True
        Me.Lbl_Veuillez_nous_excuser.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Lbl_Veuillez_nous_excuser.Font = New System.Drawing.Font("Century Gothic", 58.0!)
        Me.Lbl_Veuillez_nous_excuser.ForeColor = System.Drawing.Color.White
        Me.Lbl_Veuillez_nous_excuser.Location = New System.Drawing.Point(113, 83)
        Me.Lbl_Veuillez_nous_excuser.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Lbl_Veuillez_nous_excuser.Name = "Lbl_Veuillez_nous_excuser"
        Me.Lbl_Veuillez_nous_excuser.Size = New System.Drawing.Size(834, 93)
        Me.Lbl_Veuillez_nous_excuser.TabIndex = 0
        Me.Lbl_Veuillez_nous_excuser.Text = "Veuillez nous excuser"
        Me.Lbl_Veuillez_nous_excuser.Visible = False
        '
        'NbJoueur
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1086, 658)
        Me.Controls.Add(Me.Lbl_Veuillez_nous_excuser)
        Me.Controls.Add(Me.Lbl_IA_en_cours_de_developpement)
        Me.Controls.Add(Me.Panel_sombre_couvrant)
        Me.Controls.Add(Me.NbJoueurs)
        Me.Controls.Add(Me.BtnListeDeroul)
        Me.Controls.Add(Me.Btn_Retour)
        Me.Controls.Add(Me.Btn_Next)
        Me.Controls.Add(Me.Age4)
        Me.Controls.Add(Me.Age3)
        Me.Controls.Add(Me.Age2)
        Me.Controls.Add(Me.Age1)
        Me.Controls.Add(Me.lblAGE3)
        Me.Controls.Add(Me.lblAGE2)
        Me.Controls.Add(Me.lblAGE4)
        Me.Controls.Add(Me.lblAGE1)
        Me.Controls.Add(Me.Pseudo4)
        Me.Controls.Add(Me.Pseudo3)
        Me.Controls.Add(Me.Pseudo2)
        Me.Controls.Add(Me.Pseudo1)
        Me.Controls.Add(Me.J3)
        Me.Controls.Add(Me.J2)
        Me.Controls.Add(Me.J4)
        Me.Controls.Add(Me.J1)
        Me.Controls.Add(Me.ListeNbJoueurs)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "NbJoueur"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Saisie_info_utilisateur"
        CType(Me.BtnListeDeroul, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.codingpcimage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel_sombre_couvrant.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub

    Friend WithEvents ListeNbJoueurs As ComboBox
    Friend WithEvents J1 As Label
    Friend WithEvents J4 As Label
    Friend WithEvents J2 As Label
    Friend WithEvents J3 As Label
    Friend WithEvents Pseudo1 As TextBox
    Friend WithEvents Pseudo2 As TextBox
    Friend WithEvents Pseudo3 As TextBox
    Friend WithEvents Pseudo4 As TextBox
    Friend WithEvents lblAGE3 As Label
    Friend WithEvents lblAGE2 As Label
    Friend WithEvents lblAGE4 As Label
    Friend WithEvents lblAGE1 As Label
    Friend WithEvents Age4 As TextBox
    Friend WithEvents Age3 As TextBox
    Friend WithEvents Age2 As TextBox
    Friend WithEvents Age1 As TextBox
    Friend WithEvents Btn_Next As Button
    Friend WithEvents Btn_Retour As Button




    Friend WithEvents BtnListeDeroul As PictureBox
    Friend WithEvents NbJoueurs As Label
    Friend WithEvents codingpcimage As PictureBox
    Friend WithEvents Lbl_IA_en_cours_de_developpement As Label
    Friend WithEvents Panel_sombre_couvrant As Panel
    Friend WithEvents Lbl_Veuillez_nous_excuser As Label
End Class
