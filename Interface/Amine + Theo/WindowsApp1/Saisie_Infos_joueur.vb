﻿Imports Qwirkle
Public Class NbJoueur

    Dim Joueur1 As Joueur
    Dim Joueur2 As Joueur
    Dim Joueur3 As Joueur
    Dim Joueur4 As Joueur

    Private Sub nom_joueur_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Menu_Principale.Close()

    End Sub

    Private Sub Btn_Retour_Click(sender As Object, e As EventArgs) Handles Btn_Retour.Click
        Me.Close()
        Menu_Principale.Show()

    End Sub

    Private Sub ListeNbJoueurs_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles ListeNbJoueurs.SelectionChangeCommitted

        'Lorsque l'on selectionne le mode 1 joueur
        If sender.text = 1 Then

            'Un '1' s'affiche en tant que Nombre de Joueurs choisi
            NbJoueurs.Text = "1"

            'le texte "Pseudos des joueurs" devient blanc et perd son gris
            Label2.ForeColor = Color.White

            'Un message d'erreur s'affiche
            Lbl_Veuillez_nous_excuser.Visible = True
            Lbl_IA_en_cours_de_developpement.Visible = True
            codingpcimage.Visible = True
            Panel_sombre_couvrant.Visible = True

            'Tous les éléments de la ligne du Joueur1 sont désactivés et assombris
            Pseudo1.Enabled = False
            Pseudo1.BorderStyle = BorderStyle.None
            Pseudo1.BackColor = Color.FromArgb(6, 10, 24)
            J1.ForeColor = Color.FromArgb(6, 10, 24)
            Age1.Enabled = False
            Age1.ForeColor = Color.FromArgb(6, 10, 24)
            Age1.BorderStyle = BorderStyle.None
            Age1.BackColor = Color.FromArgb(6, 10, 24)
            lblAGE1.ForeColor = Color.FromArgb(6, 10, 24)
            Pseudo1.Text = ""
            Age1.Text = ""

            'Tous les éléments de la ligne du Joueur2 sont désactivés et assombris
            Pseudo2.Enabled = False
            Pseudo2.BorderStyle = BorderStyle.None
            Pseudo2.BackColor = Color.FromArgb(6, 10, 24)
            J2.ForeColor = Color.FromArgb(6, 10, 24)
            Age2.Enabled = False
            Age2.ForeColor = Color.FromArgb(6, 10, 24)
            Age2.BorderStyle = BorderStyle.None
            Age2.BackColor = Color.FromArgb(6, 10, 24)
            lblAGE2.ForeColor = Color.FromArgb(6, 10, 24)
            Pseudo2.Text = ""
            Age2.Text = ""

            'Tous les éléments de la ligne du Joueur3 sont désactivés et assombris
            Pseudo3.Enabled = False
            Pseudo3.BorderStyle = BorderStyle.None
            Pseudo3.BackColor = Color.FromArgb(6, 10, 24)
            J3.ForeColor = Color.FromArgb(6, 10, 24)
            Age3.Enabled = False
            Age3.ForeColor = Color.FromArgb(6, 10, 24)
            Age3.BorderStyle = BorderStyle.None
            Age3.BackColor = Color.FromArgb(6, 10, 24)
            lblAGE3.ForeColor = Color.FromArgb(6, 10, 24)
            Pseudo3.Text = ""
            Age3.Text = ""

            'Tous les éléments de la ligne du Joueur4 sont désactivés et assombris
            Pseudo4.Enabled = False
            Pseudo4.BorderStyle = BorderStyle.None
            Pseudo4.BackColor = Color.FromArgb(6, 10, 24)
            J4.ForeColor = Color.FromArgb(6, 10, 24)
            Age4.Enabled = False
            Age4.ForeColor = Color.FromArgb(6, 10, 24)
            Age4.BorderStyle = BorderStyle.None
            Age4.BackColor = Color.FromArgb(6, 10, 24)
            lblAGE4.ForeColor = Color.FromArgb(6, 10, 24)
            Pseudo4.Text = ""
            Age4.Text = ""

            'Le bouton Next disparait
            Btn_Next.Visible = False

            'Le bouton Retour disparait
            Btn_Retour.Visible = False

            MessageBox.Show("Cliquez sur l'image pour retourner au menu")
        End If

        'Lorsque l'on selectionne le mode 2 joueur
        If sender.text = 2 Then

            'Un '2' s'affiche en tant que Nombre de Joueurs choisi
            NbJoueurs.Text = "2"

            'le texte "Pseudos des joueurs" devient blanc et perd son gris
            Label2.ForeColor = Color.White

            'On peut saisir tous les éléments de la ligne du Joueur1
            Pseudo1.Enabled = True
            Pseudo1.BorderStyle = BorderStyle.Fixed3D
            Pseudo1.BackColor = Color.White
            J1.ForeColor = Color.White
            Age1.Enabled = True
            Age1.ForeColor = Color.Black
            Age1.BorderStyle = BorderStyle.Fixed3D
            Age1.BackColor = Color.White
            lblAGE1.ForeColor = Color.White

            'On peut saisir tous les éléments de la ligne du Joueur2
            Pseudo2.Enabled = True
            Pseudo2.BorderStyle = BorderStyle.Fixed3D
            Pseudo2.BackColor = Color.White
            J2.ForeColor = Color.White
            Age2.Enabled = True
            Age2.ForeColor = Color.Black
            Age2.BorderStyle = BorderStyle.Fixed3D
            Age2.BackColor = Color.White
            lblAGE2.ForeColor = Color.White

            'Tous les éléments de la ligne du Joueur3 sont désactivés et assombris
            Pseudo3.Enabled = False
            Pseudo3.BorderStyle = BorderStyle.None
            Pseudo3.BackColor = Color.FromArgb(6, 10, 24)
            J3.ForeColor = Color.FromArgb(6, 10, 24)
            Age3.Enabled = False
            Age3.ForeColor = Color.FromArgb(6, 10, 24)
            Age3.BorderStyle = BorderStyle.None
            Age3.BackColor = Color.FromArgb(6, 10, 24)
            lblAGE3.ForeColor = Color.FromArgb(6, 10, 24)
            Pseudo3.Text = ""
            Age3.Text = ""

            'Tous les éléments de la ligne du Joueur4 sont désactivés et assombris
            Pseudo4.Enabled = False
            Pseudo4.BorderStyle = BorderStyle.None
            Pseudo4.BackColor = Color.FromArgb(6, 10, 24)
            J4.ForeColor = Color.FromArgb(6, 10, 24)
            Age4.Enabled = False
            Age4.ForeColor = Color.FromArgb(6, 10, 24)
            Age4.BorderStyle = BorderStyle.None
            Age4.BackColor = Color.FromArgb(6, 10, 24)
            lblAGE4.ForeColor = Color.FromArgb(6, 10, 24)
            Pseudo4.Text = ""
            Age4.Text = ""


            'Le bouton Next apparait
            Btn_Next.Visible = True

            'Le bouton Retour apparait
            Btn_Retour.Visible = True

            MessageBox.Show("Pour une expérience optimale , nous vous conseillons de jouer a 4 joueurs. Merci")

        End If

        'Lorsque l'on selectionne le mode 3 joueur
        If sender.text = 3 Then

            'Un '3' s'affiche en tant que Nombre de Joueurs choisi
            NbJoueurs.Text = "3"

            'le texte "Pseudos des joueurs" devient blanc et perd son gris
            Label2.ForeColor = Color.White

            'On peut saisir tous les éléments de la ligne du Joueur1
            Pseudo1.Enabled = True
            Pseudo1.BorderStyle = BorderStyle.Fixed3D
            Pseudo1.BackColor = Color.White
            J1.ForeColor = Color.White
            Age1.Enabled = True
            Age1.ForeColor = Color.Black
            Age1.BorderStyle = BorderStyle.Fixed3D
            Age1.BackColor = Color.White
            lblAGE1.ForeColor = Color.White

            'On peut saisir tous les éléments de la ligne du Joueur2
            Pseudo2.Enabled = True
            Pseudo2.BorderStyle = BorderStyle.Fixed3D
            Pseudo2.BackColor = Color.White
            J2.ForeColor = Color.White
            Age2.Enabled = True
            Age2.ForeColor = Color.Black
            Age2.BorderStyle = BorderStyle.Fixed3D
            Age2.BackColor = Color.White
            lblAGE2.ForeColor = Color.White

            'On peut saisir tous les éléments de la ligne du Joueur3
            Pseudo3.Enabled = True
            Pseudo3.BorderStyle = BorderStyle.Fixed3D
            Pseudo3.BackColor = Color.White
            J3.ForeColor = Color.White
            Age3.Enabled = True
            Age3.ForeColor = Color.Black
            Age3.BorderStyle = BorderStyle.Fixed3D
            Age3.BackColor = Color.White
            lblAGE3.ForeColor = Color.White

            'Tous les éléments de la ligne du Joueur4 sont désactivés et assombris
            Pseudo4.Enabled = False
            Pseudo4.BorderStyle = BorderStyle.None
            Pseudo4.BackColor = Color.FromArgb(6, 10, 24)
            J4.ForeColor = Color.FromArgb(6, 10, 24)
            Age4.Enabled = False
            Age4.ForeColor = Color.FromArgb(6, 10, 24)
            Age4.BorderStyle = BorderStyle.None
            Age4.BackColor = Color.FromArgb(6, 10, 24)
            lblAGE4.ForeColor = Color.FromArgb(6, 10, 24)
            Pseudo4.Text = ""
            Age4.Text = ""


            'Le bouton Next apparait
            Btn_Next.Visible = True

            'Le bouton Retour apparait
            Btn_Retour.Visible = True

            MessageBox.Show("Pour une expérience optimale , veuillez jouer a 4 joueurs. Merci")

        End If

        'Lorsque l'on selectionne le mode 4 joueur
        If sender.text = 4 Then

            'Un '4' s'affiche en tant que Nombre de Joueurs choisi
            NbJoueurs.Text = "4"

            'le texte "Pseudos des joueurs" devient blanc et perd son gris
            Label2.ForeColor = Color.White

            'On peut saisir tous les éléments de la ligne du Joueur1
            Pseudo1.Enabled = True
            Pseudo1.BorderStyle = BorderStyle.Fixed3D
            Pseudo1.BackColor = Color.White
            J1.ForeColor = Color.White
            Age1.Enabled = True
            Age1.ForeColor = Color.Black
            Age1.BorderStyle = BorderStyle.Fixed3D
            Age1.BackColor = Color.White
            lblAGE1.ForeColor = Color.White

            'On peut saisir tous les éléments de la ligne du Joueur2
            Pseudo2.Enabled = True
            Pseudo2.BorderStyle = BorderStyle.Fixed3D
            Pseudo2.BackColor = Color.White
            J2.ForeColor = Color.White
            Age2.Enabled = True
            Age2.ForeColor = Color.Black
            Age2.BorderStyle = BorderStyle.Fixed3D
            Age2.BackColor = Color.White
            lblAGE2.ForeColor = Color.White

            'On peut saisir tous les éléments de la ligne du Joueur3
            Pseudo3.Enabled = True
            Pseudo3.BorderStyle = BorderStyle.Fixed3D
            Pseudo3.BackColor = Color.White
            J3.ForeColor = Color.White
            Age3.Enabled = True
            Age3.ForeColor = Color.Black
            Age3.BorderStyle = BorderStyle.Fixed3D
            Age3.BackColor = Color.White
            lblAGE3.ForeColor = Color.White

            'On peut saisir tous les éléments de la ligne du Joueur3
            Pseudo4.Enabled = True
            Pseudo4.BorderStyle = BorderStyle.Fixed3D
            Pseudo4.BackColor = Color.White
            J4.ForeColor = Color.White
            Age4.Enabled = True
            Age4.ForeColor = Color.Black
            Age4.BorderStyle = BorderStyle.Fixed3D
            Age4.BackColor = Color.White
            lblAGE4.ForeColor = Color.White


            'Le bouton Next apparait
            Btn_Next.Visible = True

            'Le bouton Retour apparait
            Btn_Retour.Visible = True

        End If


    End Sub

    'Liste Nombre de joueurs se déroule
    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles BtnListeDeroul.Click

        ListeNbJoueurs.DroppedDown = True

    End Sub
    'Liste Nombre de joueurs se déroule
    Private Sub NbJoueurs_Click(sender As Object, e As EventArgs) Handles NbJoueurs.Click

        ListeNbJoueurs.DroppedDown = True

    End Sub
    'Le message d'erreur disparait
    Private Sub Codingpcimage_Click(sender As Object, e As EventArgs) Handles codingpcimage.Click

        Panel_sombre_couvrant.Visible = False
        Lbl_Veuillez_nous_excuser.Visible = False
        Lbl_IA_en_cours_de_developpement.Visible = False

    End Sub

    'Le message d'erreur disparait
    Private Sub Lbl_IA_en_cours_de_developpement_Click(sender As Object, e As EventArgs) Handles Lbl_IA_en_cours_de_developpement.Click

        Panel_sombre_couvrant.Visible = False
        Lbl_Veuillez_nous_excuser.Visible = False
        Lbl_IA_en_cours_de_developpement.Visible = False

    End Sub

    'Le message d'erreur disparait
    Private Sub Label3_Click(sender As Object, e As EventArgs) Handles Lbl_Veuillez_nous_excuser.Click

        Panel_sombre_couvrant.Visible = False
        Lbl_Veuillez_nous_excuser.Visible = False
        Lbl_IA_en_cours_de_developpement.Visible = False

    End Sub

    Private Sub Btn_Next_Click(sender As Object, e As EventArgs) Handles Btn_Next.Click
        If CInt(ListeNbJoueurs.Text) = 2 Then
            Joueur1 = New Joueur(Pseudo1.Text, CInt(Age1.Text))
            Joueur2 = New Joueur(Pseudo2.Text, CInt(Age2.Text))
        ElseIf CInt(ListeNbJoueurs.Text) = 3 Then
            Joueur1 = New Joueur(Pseudo1.Text, CInt(Age1.Text))
            Joueur2 = New Joueur(Pseudo2.Text, CInt(Age2.Text))
            Joueur3 = New Joueur(Pseudo3.Text, CInt(Age3.Text))
        ElseIf CInt(ListeNbJoueurs.Text) = 4 Then
            Joueur1 = New Joueur(Pseudo1.Text, CInt(Age1.Text))
            Joueur2 = New Joueur(Pseudo2.Text, CInt(Age2.Text))
            Joueur3 = New Joueur(Pseudo3.Text, CInt(Age3.Text))
            Joueur4 = New Joueur(Pseudo4.Text, CInt(Age4.Text))
        End If

        TableauJeu.Show()
        Me.Hide()
    End Sub

    Public Function GetJoueur1() As Joueur
        Return Joueur1
    End Function

    Public Function GetJoueur2() As Joueur
        Return Joueur2
    End Function

    Public Function GetJoueur3() As Joueur
        Return Joueur3
    End Function

    Public Function GetJoueur4() As Joueur
        Return Joueur4
    End Function
End Class